var assert = require('assert');
var sinon = require('sinon');
var when = require('when');
var rewire = require('rewire');
var rabbitWrapper = rewire('..');

var wascally = rabbitWrapper.__get__('wascally');

describe('rabbitWrapper', function() {

    beforeEach(function() {
        sinon.stub(wascally, 'configure').returns(when(true));
        sinon.stub(wascally, 'publish').returns(when(true));
        sinon.stub(wascally, 'handle');
    });
    
    afterEach(function() {
       wascally.configure.restore(); 
       wascally.publish.restore(); 
       wascally.handle.restore();
    });

    describe('When sending first message and connecting', function () {
        it('should connect to RabbitMQ with defaults and send the message', function () {
            return rabbitWrapper.send('exchange', 'key', {
                correlationId: 'correlationId',
                message: 'message'
            }).then(function(resp) {
                assert(wascally.configure.calledOnce);
                var connection = {
                    server: 'localhost',
                    port: 5672 
                };
                assert.deepEqual(wascally.configure.args[0][0].connection, connection);
                var exchanges = [
                    { name: 'exchange', type: 'direct', persistent: true }
                ];
                assert.deepEqual(wascally.configure.args[0][0].exchanges, exchanges);
                assert(wascally.publish.calledOnce);
                assert.equal(wascally.publish.args[0][0], 'exchange');
                var message = {
                    type: 'key',
                    correlationId: 'correlationId',
                    expiresAfter: undefined,
                    timeout: 1000,
                    body: {
                        correlationId: 'correlationId',
                        message: 'message' 
                    }
                };
                assert.deepEqual(wascally.publish.args[0][1], message);
            });
        });
    });

    describe('When sending another message with no correlation id and with ttl', function () {
        it('should skip connecting to RabbitMQ and send the message', function () {
            return rabbitWrapper.send('exchange', 'key', {
                message: 'message'
            }, { ttl: 100 }).then(function(resp) {
                assert(!wascally.configure.calledOnce);
                assert(wascally.publish.calledOnce);
                assert.equal(wascally.publish.args[0][0], 'exchange');
                var message = {
                    type: 'key',
                    correlationId: undefined,
                    expiresAfter: 100,
                    timeout: 1000,
                    body: {
                        message: 'message' 
                    }
                };
                assert.deepEqual(wascally.publish.args[0][1], message);
            });
        });
    });

    describe('When sending first message and connecting with environment vars', function () {
        it('should connect to RabbitMQ with environment vars and send message', function () {
            process.env.RABBITMQ_HOST = 'host';
            process.env.RABBITMQ_PORT = 'port';

            // reset connection
            rabbitWrapper.__set__('connected', false);

            return rabbitWrapper.send('exchange', 'key', {
                correlationId: 'correlationId',
                message: 'message'
            }).then(function(resp) {
                assert(wascally.configure.calledOnce);
                var connection = {
                    server: 'host',
                    port: 'port' 
                };
                assert.deepEqual(wascally.configure.args[0][0].connection, connection);
                assert(wascally.publish.calledOnce);
            });
        });
    });

    describe('When receiving messages and connecting with environment vars', function () {
        it('should connect to RabbitMQ with environment vars and create handler', function () {
            process.env.RABBITMQ_HOST = 'host';
            process.env.RABBITMQ_PORT = 'port';

            // reset connection
            rabbitWrapper.__set__('connected', false);

            return rabbitWrapper.receive('exchange', 'queue', 'key', 'func').then(function(resp) {
                assert(wascally.configure.calledOnce);
                var connection = {
                    server: 'host',
                    port: 'port' 
                };
                var configArg = wascally.configure.args[0][0];
                assert.deepEqual(configArg.connection, connection);
                assert.deepEqual(configArg.exchanges[0], { type: 'direct', persistent: true, name: 'exchange' });
                assert.deepEqual(configArg.queues[0], { durable: true, subscribe: true, name: 'queue' });
                assert.deepEqual(configArg.bindings[0], { exchange: 'exchange', target: 'queue', keys: [ 'key' ] });
                assert(wascally.handle.calledOnce);
                assert.equal(wascally.handle.args[0][0], 'key');
            });
        });
    });

    describe('When receiving messages and connecting with environment vars using a dead letter exchange and limit', function () {
        it('should connect to RabbitMQ with environment vars and create handler', function () {
            process.env.RABBITMQ_HOST = 'host';
            process.env.RABBITMQ_PORT = 'port';

            // reset connection
            rabbitWrapper.__set__('connected', false);

            return rabbitWrapper.receive('exchange', 'queue', 'key', 'func', { dlx: 'dlx', limit: 'limit' }).then(function(resp) {
                assert(wascally.configure.calledOnce);
                var connection = {
                    server: 'host',
                    port: 'port' 
                };
                var configArg = wascally.configure.args[0][0];
                assert.deepEqual(configArg.connection, connection);
                assert.deepEqual(configArg.exchanges[0], { type: 'direct', persistent: true, name: 'exchange' });
                assert.deepEqual(configArg.queues[0], { deadLetter: 'dlx', limit: 'limit', durable: true, subscribe: true, name: 'queue' });
                assert.deepEqual(configArg.bindings[0], { exchange: 'exchange', target: 'queue', keys: [ 'key' ] });
                assert(wascally.handle.calledOnce);
                assert.equal(wascally.handle.args[0][0], 'key');
            });
        });
    });

    describe('When subscribing messages and connecting with environment vars', function () {
        it('should connect to RabbitMQ with environment vars and create handler', function () {
            process.env.RABBITMQ_HOST = 'host';
            process.env.RABBITMQ_PORT = 'port';

            // reset connection
            rabbitWrapper.__set__('connected', false);

            return rabbitWrapper.subscribe('exchange', 'queue', 'type', 'func').then(function(resp) {
                assert(wascally.configure.calledOnce);
                var connection = {
                    server: 'host',
                    port: 'port' 
                };
                var configArg = wascally.configure.args[0][0];
                assert.deepEqual(configArg.connection, connection);
                assert.deepEqual(configArg.exchanges[0], { type: 'fanout', persistent: true, name: 'exchange' });
                assert.deepEqual(configArg.queues[0], { durable: true, subscribe: true, name: 'queue' });
                assert.deepEqual(configArg.bindings[0], { exchange: 'exchange', target: 'queue', keys: [  ] });
                assert(wascally.handle.calledOnce);
                assert.equal(wascally.handle.args[0][0], 'type');
            });
        });
    });

    describe('When subscribing messages and connecting with environment vars using a dead letter exchange and limit', function () {
        it('should connect to RabbitMQ with environment vars and create handler', function () {
            process.env.RABBITMQ_HOST = 'host';
            process.env.RABBITMQ_PORT = 'port';

             //reset connection
            rabbitWrapper.__set__('connected', false);

            return rabbitWrapper.subscribe('exchange', 'queue', 'type', 'func', { dlx: 'dlx', limit: 'limit' }).then(function(resp) {
                assert(wascally.configure.calledOnce);
                var connection = {
                    server: 'host',
                    port: 'port' 
                };
                var configArg = wascally.configure.args[0][0];
                assert.deepEqual(configArg.connection, connection);
                assert.deepEqual(configArg.exchanges[0], { type: 'fanout', persistent: true, name: 'exchange' });
                assert.deepEqual(configArg.queues[0], { deadLetter: 'dlx', limit: 'limit', durable: true, subscribe: true, name: 'queue' });
                assert.deepEqual(configArg.bindings[0], { exchange: 'exchange', target: 'queue', keys: [  ] });
                assert(wascally.handle.calledOnce);
                assert.equal(wascally.handle.args[0][0], 'type');
            });
        });
    });
});
