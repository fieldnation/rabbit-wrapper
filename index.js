/**
 * Module exports
 */
exports.send = _send;
exports.receive = _receive;
exports.publish = _publish;
exports.subscribe = _subscribe;
exports.connect = _connect;

/**
 * Module dependencies.
 */
var wascally = require('wascally');

var connectionTypes = {
    'send/receive': {
        exchange: { type: 'direct', persistent: true },
        queue: { durable: true, subscribe: true }
    },
    'pub/sub': {
        exchange: { type: 'fanout', persistent: true },
        queue: { durable: true, subscribe: true }
    },
    'request/response': {
        exchange: { type: 'topic', persistent: true },
        queue: { durable: true, subscribe: true }
    }
};
var connected = false;

/**
 * Create the connection to RabbitMQ 
 *
 * @param {Object} [options = {}] - Option parameter
 * @param {Object} [options.exchange] - Exchange object
 * @param {Object} [options.queue] - Queue object
 * @param {Object} [options.binding] - Binding object
 * @param {string} [options.dlx] - Name of dead letter exchange
 * @return {Promise} 
 */
function _connect(options) {
    return new Promise(function(resolve, reject) {
        if (connected) {
            return resolve();
        }
        
        var opts = options || {};

        var config = {
            connection: {
                server: process.env.RABBITMQ_HOST || 'localhost',
                port: process.env.RABBITMQ_PORT || 5672
            }
        };

        if (opts.exchange) {
            config.exchanges = [
                opts.exchange
            ];
        }

        if (opts.queue) {
            if (opts.dlx) {
                opts.queue.deadLetter = opts.dlx;
            }
            config.queues = [
                opts.queue
            ];
        }

        if (opts.binding) {
            config.bindings = [
                opts.binding
            ];
        }

        wascally.configure(config).done(function() {
            connected = true;
            resolve();
        }, function(err) {
            reject(err);
        });
    });
}

/**
 * Send a message 
 *
 * @param {string} exchange - Exchange name
 * @param {string} key - Routing key
 * @param {Object} msg - Message to be sent 
 * @param {Object} [options={}] - Options object 
 * @param {number} [options.ttl] - Time-to-live of message in ms
 * @return {Promise}
 */
function _send(exchange, key, msg, options) {
    return new Promise(function(resolve, reject) {
        var opts = options || {};
        var config = {
            exchange: connectionTypes['send/receive'].exchange
        };
        config.exchange.name = exchange;

        _connect(config).then(function(resp) {
            wascally.publish(exchange, {
                type: key,
                correlationId: msg.correlationId !== undefined ? msg.correlationId.toString():undefined,
                timeout: 1000,
                expiresAfter: opts.ttl,
                body: msg
            }).done(function(resp) {
                resolve(resp);
            }, function(err) {
               reject(err);
            });
        }, function(err) {
            reject(err);
        });
    });
}

/**
 * Recieve messages using callback 
 * function to process them.
 *
 * @param {string} exchange - Exchange name
 * @param {string} queue - Queue name
 * @param {string} key - Routing key 
 * @param {Function} processMsg - Function to process messages
 * @param {Object} [options={}] - Options
 * @param {bool} [options.dlx] - Dead letter exchange 
 * @param {integer} [options.limit] - Number of messages to consume at a time
 * @return {Promise}
 */
function _receive(exchange, queue, key, processMsg, options) {
    var opts = options || {};
    var config = {
        exchange: connectionTypes['send/receive'].exchange,
        queue: connectionTypes['send/receive'].queue,
        binding: { exchange: exchange, target: queue, keys: [ key ] }
    };
    config.exchange.name = exchange;
    config.queue.name = queue;
    config.dlx = opts.dlx;

    if (opts.limit) {
        config.queue.limit = opts.limit;
    }

    wascally.handle(key, function(msg) {
        processMsg(msg.body).then(function(resp) {
            msg.ack();
        }, function(err) {
            msg.nack();
        });
    });

    return _connect(config);
}

/**
 * Publish a message 
 *
 * @param {string} exchange - Exchange name
 * @param {string} type - Message type 
 * @param {object} msg - Message to send
 * @param {Object} [options={}] - Options object 
 * @param {number} [options.ttl] - Time-to-live of message in ms
 * @return {Promise}
 */
function _publish(exchange, type, msg, options) {
    return new Promise(function(resolve, reject) {
        var opts = options || {};
        var config = {
            exchange: connectionTypes['pub/sub'].exchange
        };
        config.exchange.name = exchange;

        _connect(config).then(function(resp) {
            wascally.publish(exchange, {
                type: type,
                correlationId: msg.correlationId !== undefined ? msg.correlationId.toString():undefined,
                timeout: 1000,
                expiresAfter: opts.ttl,
                body: msg
            }).done(function(resp) {
                resolve(resp);
            }, function(err) {
               reject(err);
            });
        }, function(err) {
            reject(err);
        });
    });
}

/**
 * Subscribe to  messages using callback 
 * function to process them.
 *
 * @param {string} exchange - Exchange name
 * @param {string} queue - Queue name
 * @param {string} type - Message type 
 * @param {Function} processMsg - Function to process messages
 * @param {Object} [options={}] - Options
 * @param {bool} [options.dlx] - Dead letter exchange 
 * @param {integer} [options.limit] - Number of messages to consume at a time
 * @return {Promise}
 */
function _subscribe(exchange, queue, type, processMsg, options) {
    var opts = options || {};
    var config = {
        exchange: connectionTypes['pub/sub'].exchange,
        queue: connectionTypes['pub/sub'].queue,
        binding: { exchange: exchange, target: queue, keys: [ ] }
    };
    config.exchange.name = exchange;
    config.queue.name = queue;
    config.dlx = opts.dlx;

    if (opts.limit) {
        config.queue.limit = opts.limit;
    }

    wascally.handle(type, function(msg) {
        processMsg(msg.body).then(function(resp) {
            msg.ack();
        }, function(err) {
            msg.nack();
        });
    });

    return _connect(config);
}

/**
 * Send a request 
 *
 * @param {string} exchange - Exchange name
 * @param {string} key - Routing key 
 * @param {Object} msg - Message to send 
 * @param {Object} [options={}] - Options object 
 * @param {number} [options.ttl] - Time-to-live of message in ms
 * @return {Promise}
 */
function _request(exchange, key, msg, options) {
    return new Promise(function(resolve, reject) {
        var opts = options || {};
        var config = {
            exchange: connectionTypes['request/response'].exchange
        };
        config.exchange.name = exchange;

        _connect(config).then(function(resp) {
            wascally.publish(exchange, {
                type: type,
                correlationId: msg.correlationId !== undefined ? msg.correlationId.toString():undefined,
                timeout: 1000,
                expiresAfter: opts.ttl,
                body: msg
            }).done(function(resp) {
                resolve(resp);
            }, function(err) {
               reject(err);
            });
        }, function(err) {
            reject(err);
        });
    });
}

/**
 * Respond to a  message using callback 
 * function to process them.
 *
 * @param {string} exchange - Exchange name
 * @param {string} queue - Queue name
 * @param {string} key - Routing key 
 * @param {Function} processMsg - Function to process messages
 * @return {Promise}
 */
function _response(exchange, queue, key, processMsg) {
    var config = {
        exchange: connectionTypes['request/response'].exchange,
        queue: connectionTypes['request/response'].queue,
        binding: { exchange: exchange, target: queue, keys: [ key ] }
    };
    config.exchange.name = exchange;
    config.queue.name = queue;

    wascally.handle(key, function(msg) {
        processMsg(msg.body).then(function(resp) {
            msg.reply(resp);
        }, function(err) {
            msg.nack();
        });
    });

    return _connect(config);
}

