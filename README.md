# Rabbit Wrapper
Rabbit Wrapper is an opinionated promise based RabbitMQ library powered by [Wascally](https://github.com/LeanKit-Labs/wascally).

It contains convience methods for doing send/receive, pub/sub and request/response style message handling.

## Install
`npm install`

### Configuration
To modify the default settings set the following environment variables. Defaults are provided below.

```
RABBITMQ_HOST=localhost
RABBITMQ_PORT=9200
```

## API

```js
var rabbitWrapper = require('rabbit-wrapper');
```

### send(exchange, key, msg, [options])

Send a message for a send/recieve style messaging. Returns a promise.
```js
rabbitWrapper.send('example.exchange', 'example.key', { some: 'message' }).then(function(resp) {
    console.log('Message sent.');  
});
```

#### options.ttl

The time-to-live in milliseconds of the message you are sending. 

### receive(exchange, queue, key, processMsg, [options])

Receive a message for a send/recieve style messaging. Returns a promise that resolves if ready
to receive messages. Requires a function to process the message that needs to return a promise. 
```js
var processMsg = function(msg) {
    return new Promise(function(resolve, reject) {
        // Do something with msg
        resolve();
        // To nack the message call reject(err)
    };
};

rabbitWrapper.receive('example.exchange', 'example.queue', 'example.key', processMsg).then(function() {
    console.log('Ready to receive messages.');  
});
```
#### options.dlx

Dead-letter exchange name. Messages whose TTL expires will be sent to this exchange.

#### options.limit

The number of messages to consume at a time.

### publish(exchange, type, msg, [options])

Publish a message for a pub/sub style messaging. Returns a promise.
```js
rabbitWrapper.publish('example.exchange', 'example.type', { some: 'message' }).then(function(resp) {
    console.log('Message published.');  
});
```
#### options.ttl

The time-to-live in milliseconds of the message you are sending. 

### subscribe(exchange, queue, type, processMsg, [options])

Subscribe to receive messages for a pub/sub style messaging. Returns a promise that resolves if ready
to receive messages. Requires a function to process the message that needs to return a promise. 
```js
var processMsg = function(msg) {
    return new Promise(function(resolve, reject) {
        // Do something with msg
        resolve();
        // To nack the message call reject(err)
    };
};

rabbitWrapper.subscribe('example.exchange', 'example.queue', 'example.type', processMsg).then(function() {
    console.log('Ready to receive messages.');  
});
```
#### options.dlx

Dead-letter exchange name. Messages whose TTL expires will be sent to this exchange.

#### options.limit

The number of messages to consume at a time.

### request(exchange, key, msg, [options])

Send a request for a request/response style messaging. Returns a promise that resolves with the response.
```js
rabbitWrapper.request('example.exchange', 'example.key', { some: 'message' }).then(function(resp) {
    console.log(resp);  
});
```

#### options.ttl

The time-to-live in milliseconds of the message you are sending. 

### response(exchange, queue, key, processMsg)

Response to a request for a request/repsonse style messaging. Returns a promise that resolves if ready
to receive messages. Requires a function to process the message that needs to return a promise. 
```js
var processMsg = function(msg) {
    return new Promise(function(resolve, reject) {
        // Do something with msg
        resolve();
        // To nack the message call reject(err)
    };
};

rabbitWrapper.response('example.exchange', 'example.queue', 'example.key', processMsg).then(function() {
    console.log('Ready to receive messages.');  
});
```

## TTL and Dead Letter Exchanges/Queues
To utlize a dead-letter queue you must specify a TTL (time-to-live) for the messages your are sending, specify an dead-letter
exchange (dlx) and subscribe to the dlx with a queue. Example:

```js
// Send message code
rabbitWrapper.send('example.exchange', 'example.key', { some: 'message' }, { ttl: 1000 }).then(function(resp) {
    console.log('Message sent.');  
});

// Recieve message worker
var processMsg = function(msg) {
    console.log(msg);
};

rabbitWrapper.receive('example.exchange', 'example.queue', 'example.key', processMsg, { dlx: 'example.exchange.dead' }).then(function() {
    console.log('Ready to receive messages.');  
});

// Dead letter worker
var processDeadMsg = function(msg) {
    console.log(msg);
};

rabbitWrapper.subscribe('example.exchange.dead', 'example.queue.dead', 'example.key', processDeadMsg).then(function() {
    console.log('Ready to receive messages.');  
});
```

## Testing

Test are written using [Mocha](https://mochajs.org) and can be run with the following:

```
npm test
```

Generate coverage report in the `coverage` directory:
```
npm run test-cov
```
